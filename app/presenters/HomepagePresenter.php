<?php

namespace App;

use Nette,
	Model,
	Nette\Http\Url,
	Nette\Diagnostics\Debugger,
	Nette\Utils\Strings;



/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	public function renderDefault() {		
		$session = $this->context->session;		
		$conversions = $this->context->conversions;
		//set empty vals
		$this->template->bar = false;
		$this->template->progress = false;
		$this->template->videoId = false;
		$this->template->download = false;
		$this->template->data = false;
		$this->template->avgtime = $conversions->getInfo();


		if(isset($session->getSection('conversions')->videoId)) {
			$selection = $session->getSection($session->getSection('conversions')->videoId);

			$videoId = $selection->videoId;

			$this->template->bar = $selection->bar;
			$this->template->progress = $selection->progress;
			$this->template->videoId = $session->getSection('conversions')->videoId;
			$this->template->download = $selection->download;
			$this->template->data = $selection->data;
		}


	}

	protected function createComponentProcessForm()	{
		$form = new Nette\Application\UI\Form;

		$form->getElementPrototype()->class[] = "ajax";
		$form->getElementPrototype()->class[] = "form-search";

		$form->addText('link', '')
			->setRequired('Please enter link')
			->setAttribute('class', 'input-large search-query')
			->setAttribute('placeholder', 'Here paste a youtube link');

		$form->addSubmit('proccess', 'Proccess')
			->setAttribute('class','btn-large btn-primary');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = $this->proccessFormSucceeded;

		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = NULL;
		$renderer->wrappers['label']['container'] = NULL;
		$renderer->wrappers['control']['container'] = NULL;
		return $form;
	}

	public function cancel() {
		if ($this->isAjax()) {
	        $this->invalidateControl();
	    }
	    else $this->redirect('Homepage:');
	}

	public function proccessFormSucceeded($form)	{
		$values = $form->getValues();
		$link = $values->link;

		$url = new Url($link);

		//is it YT link?
		if(($url->authority != "www.youtube.com") && ($url->authority != "youtube.com")) {
			$this->flashMessage('Wrong host','error');
			$this->cancel();
			$ok = false;
		}

		//looking for v param
		else {
			$params = explode("&", $url->query);
			$ok = false;
			foreach ($params as $key => $value) {			
				if(preg_match("/v=/i", $value)) {
					$ok = true;
					$videoId = substr($value, 2);
					$param=$value;	
				}
			}

			if(!$ok) {
				$this->flashMessage('Link is wrong.','error');
				$this->cancel();
			}
			
		}


		if($ok) {
			//information from YT
			try {
				$info = file_get_contents("https://gdata.youtube.com/feeds/api/videos/".$videoId."?v=2");
				$xml = simplexml_load_string($info);
				$name  = (string) $xml->title;
				$name = Strings::webalize($name);
			}
			catch(Exception $e) {				
				$this->flashMessage('Error by retriving information from server.','error');
				$this->cancel();
			}


			$conversions = $this->context->conversions;

			//checking in database
			$entry = $conversions->getByVideoId($videoId);
			if($entry && file_exists(TMP_DIR."mp3/".$entry->filename."-".$entry->videoId.".mp3")) {
				$this->redirect('Homepage:download',$videoId);
			}

			elseif ($conversions->getJobByVideoId($videoId) && ($conversions->getJobByVideoId($videoId)->done == 0)) {
				$this->flashMessage("This file is acctually converting. Please try again in 5 mins",'info');
				$this->cancel();
			}

			
			else {
				//making actual job
				$conversions->startJob($videoId);

				$link = 'http://www.youtube.com/watch?v='.$videoId;

				//converting
				$cmd = 'sh '.BIN_DIR.'youtube2mp3.sh "'.$link.'" "'.$name.'-'.$videoId.'" "'.TMP_DIR.'"';

				$hash = sha1($videoId);

				$out = TMP_DIR.'out/'.$hash;
				$pid = TMP_DIR.'pid/'.$hash;

				exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $out, $pid));

				$data = array(
					'filename' => $name,
					'videoId' => $videoId
				);


				$session = $this->context->session;
				$session->getSection('conversions')->videoId = $videoId;
				$selection = $session->getSection($videoId);

				$selection->videoId = $videoId;
				$selection->bar = true;
				$selection->data = $data;
				$selection->download = false;

				$this->flashMessage("Starting conversion for ".$xml->title,'info');
				$this->cancel();
			}
		}
	}

	public function handleStatus($videoId) {
		$session = $this->context->session;
		$selection = $session->getSection($videoId);

		$hash = sha1($videoId);
		$lines = file_get_contents(TMP_DIR.'out/'.$hash);
		$progress = 0;

		if(preg_match('/download/i', $lines)) {		

			$downloaded = explode("[download]",$lines);
			foreach ($downloaded as $value) {
				$progress = (int) @round(substr(trim($value),0,3));
			}
		};

		if($progress >= 34) {
			$progress -=3;
		}

		if($progress >= 74) {
			$progress -=2;
		}

		if(preg_match('/global headers/i', $lines)) {
			$progress = 100;
		}

		$selection->progress = $progress;
		if(
			$selection->progress >= 100) {
			$selection->progress = 0;
			$selection->bar = false;
		

			//databsae
			$conversions = $this->context->conversions;
			$selection->data['saved'] = new \Datetime;
			$conversions->save($selection->data);
			$conversions->closeJobByVideoId($videoId);

			//clearing
			unlink(TMP_DIR.'out/'.$hash);
			unlink(TMP_DIR.'pid/'.$hash);

			$selection->download = true;
		
		}

		$this->cancel();
	}



	public function actionDownload($videoId) {	

		$session = $this->context->session;
		$selection = $session->getSection('conversions');
		unset($selection->videoId);

		$conversions = $this->context->conversions;
		//checking in database
		$entry = $conversions->getByVideoId($videoId);

		$file = TMP_DIR."mp3/".$entry->filename."-".$entry->videoId.".mp3"; 
		
		$httpResponse = $this->context->getService('httpResponse');
        $httpResponse->setHeader('Pragma', "public");
        $httpResponse->setHeader('Expires', 0);
        $httpResponse->setHeader('Cache-Control', "must-revalidate, post-check=0, pre-check=0");
        $httpResponse->setHeader('Content-Transfer-Encoding', "binary");
        $httpResponse->setHeader('Content-Description', "File Transfer");
        $httpResponse->setHeader('Content-Length', filesize($file));
        $this->sendResponse(new \Nette\Application\Responses\FileResponse($file, $entry->filename."-".$entry->videoId.".mp3", mime_content_type($file)));

	}

}
