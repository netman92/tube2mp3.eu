<?php

namespace App;

use Nette,
	Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	public function beforeRender()
	{
	    if ($this->isAjax()) {
	        $this->invalidateControl('flashMessages');
	    }

	    $httpRequest = $this->context->getService('httpRequest');
	    $this->template->host = $httpRequest->getUrl()->host;
	}
}
