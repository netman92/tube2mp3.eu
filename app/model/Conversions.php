<?php

namespace Model;

use Nette,
	Nette\Database\Connection,
	Nette\Diagnostics\Debugger;
class Conversions extends Nette\Object
{
    /** @var Nette\Database\Connection */
    protected $db;

    public function __construct(Nette\Database\Connection $db)
    {
        $this->db = $db;
    }    

    public function save($data) {
		return $this->db->exec('INSERT INTO conversion', $data);
	}

	public function getByVideoId($videoId) {
		return $this->db->table('conversion')
			->select('*')
			->where('videoId',$videoId)
			->limit(1)
			->fetch();
	}

	public function startJob($videoId) {
		$data = array(
			'videoId' => $videoId,
			'started' => new \Datetime
		);

		return $this->db->exec('INSERT INTO jobs', $data);
	}

	public function getJobByVideoId($videoId) {
		return $this->db->table('jobs')
			->select('*')
			->where('videoId',$videoId)
			->limit(1)
			->fetch();
	}

	public function closeJobByVideoId($videoId) {
		$data = array('done' => 1);
		return $this->db->exec('UPDATE jobs SET ? WHERE videoId=?', $data, $videoId);
	}

	public function getInfo() {
		$q = $this->db->query('SELECT AVG(TIMEDIFF(saved,started)) as diff, COUNT(*) as count FROM jobs JOIN conversion ON jobs.videoId = conversion.videoId')->fetch();

		$sec = $q->diff;

		$min = 0;
		if($sec >= 60) {			
			$min = floor($sec/60);
			$sec -= $min * 60;
			$sec = round($sec,2);
		}

		return 'Average time of converting is '.$min.' min and '.$sec
				.' sec. Totally converted '.$q->count . ' files.';
	}


}
