#!/bin/bash

#$1 = url to YT
#$2 = title of video
#$3 = path to save with /

youtube-dl $1 -o "${3}${2}.flv"
avconv -i "${3}${2}.flv" "${3}mp3/${2}.mp3"
rm -rf "${3}${2}.flv"



