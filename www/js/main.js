jQuery.ajaxSetup({
    cache: false,
    dataType: 'json',
    success: function (payload) {
    	// redirect
        if (payload.redirect) {
            window.location.href = payload.redirect;
            return;
        }

        if (payload.snippets) {
            for (var i in payload.snippets) {
            	$('#' + i).hide();
                $('#' + i).html(payload.snippets[i]);
                $('#' + i).show();
            }
        }
    }
});
$(function(){
    var stopWatch = 0;
    var m = 0;
    var s = 0;
    stop = false;


	// odesílání odkazů
	$('a.ajax').live('click', function (event) {
	    event.preventDefault();
	    $.get(this.href);
	});

	// odesílání formulářů
	$('form.ajax').live('submit', function (event) {
	    event.preventDefault();
	    $.post(this.action, $(this).serialize());
	});
 
    function update() {
        if((!stopWatch) && $("#bar").is(":visible")) {
            stopWatchRun();
            stopWatch = 1;
            s = 0;
            m = 0;
        } 
        $("#statusLink").click();
        setTimeout(update, 2000);
    }

    update();

    function stopWatchRun() {
        if(!stop) {
            s += 1;
            if(s>=60) {
                s =0;
                m +=1;
            }
            if(s<10) ss = 0; else ss = '';
            if(m<10) mm = 0; else mm = '';

            $("#stopwatch").show().html(mm+m+':'+ss+s);

            if ( $("#downloadBtn").is(":visible") ) {
                stop = true;
                s=0;
                m=0;
                $("#stopwatch").hide();
                stopWatch = 0;
            } else {             
                setTimeout(stopWatchRun, 1000);
            }
        }

    }




});