<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require '.maintenance.php';

// Let bootstrap create Dependency Injection container.
$container = require __DIR__ . '/../app/bootstrap.php';

define('BIN_DIR',__DIR__ . '/../app/bin/');
define('TMP_DIR',__DIR__ . '/../temp/');

// Run application.
$container->application->run();
